package PageFactory;

import General.Main;
import TestCases.testcases_Login;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class loginObjects extends Main {

    public static By byUserName = By.id("username");
    public static By byPassword = By.id("inputPassword3");
    public static By bysigninbutton = By.className("btn-auth");

    public loginObjects(){
        PageFactory.initElements(driver, this);
    }

    public static void EnterUserName(String Username) {
        driver.findElement(byUserName).sendKeys(Username);
    }
    public static void EnterPassword(String Password) {
        driver.findElement(byPassword).sendKeys(Password);
    }
    public static void clicksigninbutton() {
        driver.findElement(bysigninbutton).click();
    }

    public void Login() {
        EnterUserName("MH123");
        EnterPassword("Mariam90");
        clicksigninbutton();
    }


}
