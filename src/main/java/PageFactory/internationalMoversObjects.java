package PageFactory;

import General.Main;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class internationalMoversObjects extends Main{
    JavascriptExecutor jse = (JavascriptExecutor) driver;

    homeObjects homeobjects = new homeObjects();
    localMoversObjects localmovers = new localMoversObjects();


    public internationalMoversObjects(){
        this.homeobjects = homeobjects;
        this.localmovers = localmovers;
        PageFactory.initElements(driver, this);
        this.action = action;
    }

//    International Address
//    International From Address
    @FindBy(id="edit-field-international-address-from")
    public static WebElement byInternationalFromAddress;
    //    Select International From Address
    public void enterInternationalFromAddress(String fromAddress){
            byInternationalFromAddress.sendKeys(fromAddress);
    }

//    International To Address
    @FindBy(id="edit-field-international-address-to")
    public static WebElement byInternationalToAddress;
    //    Select International To Address
    public void enterInternationalToAddress(String toAddress){
        byInternationalToAddress.sendKeys(toAddress);
    }

    public void InternationalMove(){
//        Home screen
        homeobjects.clickInternationalMoving();
        homeobjects.clickGetStarted();

//        Journey1
        localmovers.openDatePicker();
        localmovers.clickNextMonthArrow();
        localmovers.clickSelectDate();

        enterInternationalFromAddress("abc");
        enterInternationalToAddress("xyz");

        jse.executeScript("arguments[0].scrollIntoView();", localmovers.byHandyMan);

        localmovers.openMovingSize();
        localmovers.clickMovingSize();

        localmovers.enterOtherDetails("New details.");

        localmovers.clickHandyMan();
        localmovers.clickStorage();

        localmovers.clickSelectMyOwnCompanies();

    }
}
