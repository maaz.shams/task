package PageFactory;
import General.Main;
import TestCases.testcases_Login;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static junit.framework.TestCase.assertTrue;


public class homeCleaningBookingObjects extends  Main {
    testcases_Login login;
    JavascriptExecutor jse = (JavascriptExecutor) driver;

    public homeCleaningBookingObjects() {
        this.login = new testcases_Login();
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#al-sidebar-list > ba-menu-item:nth-child(1)")
    public static WebElement BybookingTab;

    public void clickbookingTab() {
        BybookingTab.click();
    }

    @FindBy(css = "#al-sidebar-list > ba-menu-item:nth-child(1) > li > ul > ba-menu-item:nth-child(1)")
    public static WebElement ByOpenTab;

    public void clickOpenTab() {
        ByOpenTab.click();
    }


    @FindBy(css = "#al-sidebar-list > ba-menu-item:nth-child(1) > li > ul > ba-menu-item:nth-child(1) > li > ul > ba-menu-item:nth-child(1)")
    public static WebElement BycleaningbookingTab;

    public void clickcleaningbookingTab() {
        BycleaningbookingTab.click();
    }


    @FindBy(css = "body > app > main > pages > div > div > forms > basic-tables > div > div:nth-child(2) > ba-card > div > div > ng2-smart-table > div > table > tbody > tr.ng2-smart-row.selected > td:nth-child(1) > ng2-smart-table-cell > table-cell-view-mode > div > div")
    public static WebElement Byfirstbooking;

    public void clickfirstbooking() {
        Byfirstbooking.click();
    }

    @FindBy(xpath = "/html/body/app/main/pages/div/div/forms/booking-view/div/div[2]/div/booking-detail-cleaning/ba-card/div/div[2]/div/div[20]/div/button[2]")
    public static WebElement ByAcceptButton;

    public void clickAcceptButton() {
        ByAcceptButton.click();
    }

    @FindBy(css = "body > modal.modal.fade.in > div > div > modal-footer > div > button.btn.btn-info")
    public static WebElement ByMaidAssignlaterbutton;

    public void clickMaidAssignlaterbutton() {
        ByMaidAssignlaterbutton.click();
    }

    @FindBy(css = "body > app > main > pages > div > div > forms > booking-view > div > div:nth-child(2) > div > booking-detail-cleaning > ba-card > div > div.card-body > div > div.col-sm-12.pan > h2")
    public static WebElement BYbookingAcceptedMessage;

    public void checkbookingAcceptedMessage() {
        BYbookingAcceptedMessage.click();
    }

    @FindBy(css = "body > app > main > pages > div > div > ba-content-top > div > h1")
    public static WebElement BYCleaningbookingGridText;

    public void verifyCleaningbookingGridText() {
        BYCleaningbookingGridText.click();
    }

    public void HomeCleaningBooking() throws InterruptedException {
        login.T0001();
        clickbookingTab();
        clickOpenTab();
        clickcleaningbookingTab();
        Thread.sleep(3000);
        jse.executeScript("arguments[0].scrollIntoView();", homeCleaningBookingObjects.BybookingTab);
    }

    public void HomeCleaningWithoutMaid() throws InterruptedException {
        login.T0001();
        clickbookingTab();
        clickOpenTab();
        clickcleaningbookingTab();
        //  driver.wait(1000);
        clickfirstbooking();
        //   driver.wait(1000);
        Thread.sleep(3000);
        jse.executeScript("arguments[0].scrollIntoView();", ByAcceptButton);
        clickAcceptButton();
        Thread.sleep(3000);
        jse.executeScript("arguments[0].scrollIntoView();", ByMaidAssignlaterbutton);
        clickMaidAssignlaterbutton();
        Thread.sleep(3000);
        jse.executeScript("arguments[0].scrollIntoView();", BYbookingAcceptedMessage);
        assertTrue(driver.getPageSource().contains("Booking has been accepted successfully"));
    }

}