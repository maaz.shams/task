package PageFactory;

import General.Main;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class localMoversObjects extends Main {
    homeObjects homeobjects = new homeObjects();
    JavascriptExecutor jse = (JavascriptExecutor) driver;

    public localMoversObjects(){
        this.homeobjects = homeobjects;
        PageFactory.initElements(driver, this);
    }

//    Journey 1
//    Date Picker
//    Date Picker field
    @FindBy(id="edit-field-expected-move-date")
    public static WebElement byDatePicker;
//    Click to open Date Picker
    public void openDatePicker(){
        byDatePicker.click();
    }

//    Next Month arrow in Date Picker
    @FindBy(partialLinkText="Next")
    public static WebElement byNextMonthArrow;
//    Click on Next Month Arrow
    public void clickNextMonthArrow(){
        byNextMonthArrow.click();
    }

//    Date in Date Picker
    @FindBy(partialLinkText="20")
    public static WebElement bySelectDate;
//    Select Date
    public void clickSelectDate(){
        bySelectDate.click();
    }

//    Address
//    From Address
    @FindBy(id="edit-field-address")
    public static WebElement byFromAddress;
//    Select From Address
    public void enterFromAddress(String fromAddress){
        byFromAddress.sendKeys(fromAddress);
    }

//    To Address
    @FindBy(id="edit-field-field-address-to")
    public static WebElement byToAddress;
//    Select To Address
    public void enterToAddress(String toAddress){
        byToAddress.sendKeys(toAddress);
    }

//    Moving size
//    Moving Size dropdown
    @FindBy(css="button[data-id = edit-field-moving-size]")
    public static WebElement byOpenMovingSize;
//    Click to Open Moving Size
    public void openMovingSize(){
        byOpenMovingSize.click();
    }

//    Selection of Moving Size
    @FindBy(partialLinkText="2 Bedroom Apartment")
    public static WebElement bySelectMovingSize;
//    Select Moving Size
    public void clickMovingSize(){
        bySelectMovingSize.click();
    }

//    Other Details
    @FindBy(id="edit-field-other-details")
    public static WebElement byOtherDetails;
//    Select Other Details
    public void enterOtherDetails(String otherDetails){
        byOtherDetails.sendKeys(otherDetails);
    }

//    Service - Handyman
    @FindBy(className="icon-handyman")
    public static WebElement byHandyMan;
//    Select Service - Handyman
    public void clickHandyMan(){
        byHandyMan.click();
    }

//    Service - Storage
    @FindBy(className="icon-storage")
    public static WebElement byStorage;
//    Select Service - Storage
    public void clickStorage(){
        byStorage.click();
    }

//    I Want To Select My Companies button
    @FindBy(id="btn-select")
    public static WebElement bySelectMyOwnCompanies;
//    Select I Want To Select My Companies button
    public void clickSelectMyOwnCompanies (){
        bySelectMyOwnCompanies.click();
    }

//    Journey 2
//    Company selection
    @FindBy(className="tick")
    public static List<WebElement> byCompanyTick;
//    Select Company Tick
    public void clickCompanyTick (int i){
        byCompanyTick.get(i).click();
    }

//    Get Your Free Quote button
    @FindBy(id="btn-get-quote")
    public static WebElement byGetYourFreeQuote;
//    Select Your Free Quote button
    public void clickGetYourFreeQuote (){
        byGetYourFreeQuote.click();
    }

//    Journey 3
//    Email Address
    @FindBy(id="input-email")
    public static WebElement byInputEmail;
//    Enter Email Address
    public void enterEmail (){
        byInputEmail.sendKeys();
    }

//    Sign In Text
    @FindBy(id="sign-in-text")
    public static WebElement bySignInText;
//    Display Sign In Text
    public void displayedSignInText (){
        bySignInText.isDisplayed();
    }

//    Load Login button
    @FindBy(className="load-loginObjects-modal")
    public static WebElement byLoadLoginModal;
//    Click Load Login button
    public void clickLoadLogin (){
        byLoadLoginModal.click();
    }

    public void LocalMove(){
//        Home screen
        homeobjects.clickMovingAndStorage();
        homeobjects.clickGetStarted();

//        Journey 1
        openDatePicker();
        clickNextMonthArrow();
        clickSelectDate();

        enterFromAddress("abc");
        enterToAddress("xyz");

        jse.executeScript("arguments[0].scrollIntoView();", byHandyMan);

        openMovingSize();
        clickMovingSize();

        enterOtherDetails("New details.");

        clickHandyMan();
        clickStorage();

        clickSelectMyOwnCompanies();

//        Journey 1#2
        // Selecting the top 3 companies on the screen
        for (int i = 0; i < 3; i++) {
            clickCompanyTick(i);
            jse.executeScript("window.scrollBy(0,100)", "");
        }

//        Journey 1#3
    }

}
