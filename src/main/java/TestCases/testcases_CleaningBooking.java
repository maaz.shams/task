package TestCases;

import General.Main;
import PageFactory.homeCleaningBookingObjects;
import PageFactory.loginObjects;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;


public class testcases_CleaningBooking extends Main {

    JavascriptExecutor jse = (JavascriptExecutor) driver;
    loginObjects loginObjects = new loginObjects();
    homeCleaningBookingObjects homeCleaningBookingObjects = new homeCleaningBookingObjects();
    testcases_Login testcases_Login = new testcases_Login();

    public testcases_CleaningBooking() {
        this.homeCleaningBookingObjects = homeCleaningBookingObjects;
        this.testcases_Login = testcases_Login;
    }

    // New booking_Tab_verification_Test case
    @Test
    public void T0002() throws InterruptedException {
        Thread.sleep(5000);
        homeCleaningBookingObjects.HomeCleaningBooking();
    }

    // Booking_Acceptance_without_maid_Test case
    @Test
    public void T0003() throws InterruptedException {
        Thread.sleep(5000);
        homeCleaningBookingObjects.HomeCleaningWithoutMaid();
    }


}



