package General;

import TestCases.testcases_CleaningBooking;
import TestCases.testcases_LocalMove;
import TestCases.testcases_InternationalMove;
import TestCases.testcases_Login;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.*;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Main {
    public static WebDriver driver;
    public static Actions action;
    public static Random rand = new Random();

    testcases_LocalMove localMove;
    testcases_InternationalMove internationalMove;
    testcases_Login Logintestcase;
    testcases_CleaningBooking cleaningbooking;

    @BeforeClass
    public void beforeWebdriver() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        ChromeOptions op = new ChromeOptions();
        op.addArguments("--start-maximized");
        driver = new ChromeDriver(op);
        driver.get("https://uat1-operations.servicemarket.com");
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

//    @AfterMethod
//    public void afterWebdriver() {
//        driver.quit();
//    }

    }



